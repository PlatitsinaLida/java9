import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

//Напишите класс LambdaDemo с набором открытых статических неизменяемых полей,
// которым в качестве значений присвоены следующие лямбда-выражения.
public class LambdaDemo {

    //1) для строки символов получить ее длину
    public static final Function<String, Integer> getStringLength = string -> {
        if (string == null) {
            return null;
        }
        return string.length();
    };

    //2) для строки символов получить ее первый символ, если он существует, или null иначе
    public static final Function<String, Character> getFirstCharacterInString = string -> {
        if (string == null || "".equals(string)) {
            return null;
        }
        return string.charAt(0);
    };

    //3) для строки проверить, что она не содержит пробелов
    public static final Predicate<String> checkSpaceInString = string -> {
        if (string == null) {
            return true;
        }
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == ' ') {
                return false;
            }
        }
        return true;
    };


    //4) слова в строке разделены запятыми, по строке получить количество слов в ней
    public static final Function<String, Integer> getAmountsOfWordsInString = string -> {
        if (string == null || "".equals(string)) {
            return 0;
        }
        String[] buf = string.split(",");
        int res = 0;
        for (int i = 0; i < buf.length; i++) {
            if (!"".equals(buf[i])) {
                res++;
            }
        }
        return res;
    };

    //5) по человеку получить его возраст
    //Сделайте так, чтобы лямбда-выражения из пунктов 5) — 7) могли работать не только с Human, но и с объектами типа Student.
    public static final Function<? extends Human, Integer> getAgeOfHuman = human -> {
        if (human == null) {
            return null;
        }
        return Human.CURRENT_YEAR - human.getAge();
    };

    //6) по двум людям проверить, что у них одинаковая фамилия
    //Сделайте так, чтобы лямбда-выражения из пунктов 5) — 7) могли работать не только с Human, но и с объектами типа Student.
    public static final BiPredicate<? extends Human, ? extends Human> compareSurname = (human1, human2) -> {
        if (human1 == null || human2 == null) {
            return false;
        }
        return human1.getLastName().equals(human2.getLastName());
    };

    //7) получить фамилию, имя и отчество человека в виде одной строки (разделитель — пробел)
    //Сделайте так, чтобы лямбда-выражения из пунктов 5) — 7) могли работать не только с Human, но и с объектами типа Student.
    public static final Function<? extends Human, String> getSurnameNamePatronymic = human -> {
        if (human == null) {
            return null;
        }
        return human.getLastName() + " " + human.getFirstName() + " " + human.getMiddleName();
    };

    //сделать человека старше на один год (по объекту Human создать новый объект)
    public static final Function<Human, Human> makeOlder = human -> {
        if (human == null) {
            return null;
        }
        Human older = new Human(human);
        try {
            older.setAge(human.getAge() - 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return older;
    };

    //по трем людям и заданному возрасту maxAge проверить, что все три человека моложе maxAge
    public static final AgeChecker checkMaxAge = (human1,human2,human3,maxAge) -> {
        if (human1 == null || human2 == null || human3 == null || maxAge ==null) {
            return false;
        }
        return (maxAge > (Human.CURRENT_YEAR - human1.getAge())
                && maxAge > (Human.CURRENT_YEAR - human2.getAge())
                && maxAge > (Human.CURRENT_YEAR - human3.getAge()));
    };


}